<?php
/**
 * Created by PhpStorm.
 * User: mirekratman
 * Date: 26/06/15
 * Time: 13:09
 */

ERROR_REPORTING(E_ALL);
require_once(__DIR__ . '/../src/loader.php');

class ApiTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var $classApiClient - API Client instance
     */
    protected $classCertificate;

    public function __construct()
    {
        $this->classCertificate = new Fin\GuaranteeCertificate();
        $this->classCertificate->setIsin('US0378331005');
        $this->classCertificate->setTradingMarket('xxsc');
        $this->classCertificate->setCurrency('EUR');
        $this->classCertificate->setIssuer('Apple');
        $this->classCertificate->setIssuingPrice(23.30);
        $this->classCertificate->getDocuments()->addDocument('document1.pdf',0);
        $this->classCertificate->getDocuments()->addDocument('document1.doc',1);
        $this->classCertificate->updatePricesHistory('http://ichart.finance.yahoo.com/table.csv?s=AAPL&a=0&b=01&c=2011&d=11&e=31&f=2011&g=d');
        $this->classCertificate->setBarier(23.30);
        $this->classCertificate->setParticipationRate('50%');
    }


    /*
     * Test if ISIN is set
     */
    public function testIsinSet()
    {
        $this->assertSame('US0378331005', $this->classCertificate->getIsin());
    }


    /*
    * Test if price history is updated
    */
    public function testPriceHistory()
    {
        $this->assertGreaterThan(0, $this->classCertificate->getPriceHistory()->getPriceHistory());
    }

}