<?php
/**
 * Created by PhpStorm.
 * User: mirekratman
 * Date: 27/07/15
 * Time: 19:33
 */

ERROR_REPORTING(E_ALL);
require __DIR__ . "/../src/loader.php";

echo "<pre>";

try {
    $classCertificate = new Fin\GuaranteeCertificate();
    $classCertificate->setIsin('US0378331005');
    $classCertificate->setTradingMarket('xxsc');
    $classCertificate->setCurrency('EUR');
    $classCertificate->setIssuer('Apple');
    $classCertificate->setIssuingPrice(23.30);
    $classCertificate->getDocuments()->addDocument('document1.pdf',0);
    $classCertificate->getDocuments()->addDocument('document1.doc',1);
    $classCertificate->updatePricesHistory('http://ichart.finance.yahoo.com/table.csv?s=AAPL&a=0&b=01&c=2011&d=11&e=31&f=2011&g=d');

    $classCertificate->setBarier(23.30);
    $classCertificate->setParticipationRate('50%');

    print_r($classCertificate->getCertificateAsArray());

} catch (\Exception $e) {
    print_r($e);
}


