# Certificate repository

Here you can find  Certificate task functionality which include:
- main Certificate class (Certificate.php)
- BonusCertificate class (BonusCertificate.php)
- GuaranteeCertificate class (GuaranteeCertificate.php)

There is also some extra classes to support necesary functionality like Documents definition of Prices History.
Because of the fact that i've got response "Please, solve the case study to your best ability and knowledge."
I've tried to somehow imagine how the simply web app can work I've decided to create app the way like you can see in repo.

Generally there is few things that needs to be discused to understand more deeply the way how app needs to work
I will point some of them to show that the app can be done a bit differently. Lets see:
- one things which can be important is the structure of Certificate class and its dependency.
  When - let say - we will agree that final Certificate must have all the functionality which is included
  in to the BonusCertificate and GuaranteeCertificate then we can create BonusCertificate and/or GuaranteeCertificate
  as a interfaces for Certificate. This will allow us to define for each Certificate class the interfaces which
  which will provide necessary functionality.
- second thing is the way how we want to generate the content or even where we want or we agree to put this functionality.
  Generally this functionality (displayAsHtml(), displayAsXml()) can be a part of Certificate class, but in my opinion
  it can be a mix of a MODEL (Certificate) and a VIEW (html template, xml functionality/template). In this case Ive decided to
  move those methods in to MVC framework where - in my opinion - is the right place for defining the VIEW.
  In this case MODEL (Certificate) can exists separately and no matter how we will change the model the VIEW is separate thing
  and uses only given functionality to display data.
  It can have some disadvantages of course - like the situaction when we want only to modify model and the rest will be "displayed"
  automatically. In this case the functionality of generate HTML and/or XML needs to be added in to Certificate class but
  this also can make some issues when we again decide to limit access to the data for user by defining some rights for them.
  In this case we need to deal with MODEL differently - I mean the way how I designed the app.
- another thing is I dont know (must be defined/agreed) how to validate data delivered to Certificate. This is important
  from security side.

I've added also some simple parser for Yahoo Finance CSV feed to get some "updates" of stock prices.

# Usage
You can find some exaples of classes usage in /usage folder.

# Unit tests
In /test folder there is an exaple of unit test class with a XML file definition.

# Documentation
There is also /docs folder which contain auto-generated documentation done by PHPDocumentor. The config file is located
in root folder - phpdoc.dist.xml

# UML - generate class schema
You can also find a UML graph in /docs folder as also in /project.xmi file
The UML graph is done through phUML:
```
php vendor/jakobwesthoff/phuml/src/app/phuml -r ./src -graphviz -createAssociations false -neato ./docs/uml.png
```
the /project.xmi by PHPUML
```
phpuml src -o project.xmi
```

All libraries including phUML is attached in to /composer.json definition.
