<?php
/**
 * Created by PhpStorm.
 * User: mirekratman
 * Date: 28/07/15
 * Time: 17:35
 */

namespace Fin\Addons;

use Fin\Enums\DocumentTypes;

class Documents
{
    /*
     * Certificate documents list
     * @var array
     */
    protected $documents = array();

    /**
     * @return array
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param array $documents
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;
    }

    /**
     * Method will add document to the list and verify its format by defined type
     * @param string $file - file to add
     * @param type $typeId - file type ID
     * @return void
     */
    public function addDocument($file, $typeId)
    {
        $dt = new DocumentTypes();

        if( !$dt->isValidType($file,$typeId) ){
            throw new \Exception('Document type dont match defined type');
        }

        array_push($this->documents, array(
            'file' => $file,
            'type' => $typeId,
            'typeName' => $dt->getTypeNameById($typeId)
        ));
    }

}