<?php
/**
 * Created by PhpStorm.
 * User: mirekratman
 * Date: 28/07/15
 * Time: 17:35
 */

namespace Fin\Addons;

class PricesHistory
{
    /*
     * Price history list
     * @var array
     */
    protected $priceHistory = array();

    /**
     * @return array
     */
    public function getPriceHistory()
    {
        return $this->priceHistory;
    }

    /**
     * @param array $priceHistory
     */
    public function setPriceHistory($priceHistory)
    {
        $this->priceHistory = $priceHistory;
    }

    /**
     * @return JSON
     */
    public function getPriceHistoryAsJson()
    {
        return json_encode(self::getPriceHistory());
    }

    /**
     * @return array
     */
    public function getPriceHistoryAsArray()
    {
        return (array)self::getPriceHistory();
    }

    /**
     * Method will add price with its timestamp to history array
     * @param double $price
     * @param integer $timestamp
     * @return void
     */
    public function addPrice($price, $timestamp)
    {
        array_push($this->priceHistory, array(
            'timestamp' => $timestamp,
            'price' => $price
        ));
    }
}