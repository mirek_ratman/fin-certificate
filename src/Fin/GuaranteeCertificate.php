<?php
/**
 * Bonus Certificate class definition
 * @copyright Miroslaw Ratman
 * @author Miroslaw Ratman
 * @since 2015-07-27
 * @license MIT
 */

namespace Fin;

use Fin\BonusCertificate;

class GuaranteeCertificate extends BonusCertificate
{
    /*
     * Participation rate
     * @var double
     */
    protected $participationRate;

    /**
     * @return mixed
     */
    public function getParticipationRate()
    {
        return $this->participationRate;
    }

    /**
     * @param mixed $participationRate
     */
    public function setParticipationRate($participationRate)
    {
        $this->participationRate = $participationRate;
    }

    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

}