<?php
/**
 * Certificate class definition
 * @copyright Miroslaw Ratman
 * @author Miroslaw Ratman
 * @since 2015-07-27
 * @license MIT
 */

namespace Fin;

use Fin\Addons\Documents;
use Fin\Addons\PricesHistory;
use Fin\Helpers\YahooFinanceParser;
use Fin\Enums\TradingMarket;

class Certificate
{
    /*
     * ISIN number ID
     * @var string
     */
    private $isin;

    /*
     * Trading market ID
     * @var integer
     */
    private $tradingMarket;

    /*
     * Currency
     * @var string
     */
    private $currency;

    /*
     * Issuer
     * @var string
     */
    private $issuer;

    /*
     * Issuing price
     * @var double
     */
    private $issuingPrice;

    /*
     * Current price
     * @var double
     */
    private $price;

    /*
     * Attached documents
     * @var array
     */
    private $documents;

    /*
     * Price history
     * @var array
     */
    private $priceHistory;

    /**
     * @return mixed
     */
    public function getIsin()
    {
        return $this->isin;
    }

    /**
     * @param mixed $isin
     */
    public function setIsin($isin)
    {
        $this->isin = $isin;
    }

    /**
     * @return mixed
     */
    public function getTradingMarket()
    {
        return $this->tradingMarket;
    }

    /**
     * @param mixed $tradingMarket
     */
    public function setTradingMarket($tradingMarket)
    {
        $tm = new TradingMarket();
        if ($tm->isValidMarket($tradingMarket)) {
            $this->tradingMarket = $tm->getMarketDetailsById($tradingMarket);
        } else {
            throw new \Exception('Given Trading Market MIC ID is not a valid and not exists on list of trading markets');
        }
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getIssuer()
    {
        return $this->issuer;
    }

    /**
     * @param mixed $issuer
     */
    public function setIssuer($issuer)
    {
        $this->issuer = $issuer;
    }

    /**
     * @return mixed
     */
    public function getIssuingPrice()
    {
        return $this->issuingPrice;
    }

    /**
     * @param mixed $issuingPrice
     */
    public function setIssuingPrice($issuingPrice)
    {
        $this->issuingPrice = $issuingPrice;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param mixed $documents
     */
    public function setDocuments(Documents $documents)
    {
        $this->documents = $documents;
    }

    /**
     * @return mixed
     */
    public function getPriceHistory()
    {
        return $this->priceHistory;
    }

    /**
     * @param mixed $priceHistory
     */
    public function setPriceHistory(PricesHistory $priceHistory)
    {
        $this->priceHistory = $priceHistory;
    }

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->setDocuments(new Documents());
        $this->setPriceHistory(new PricesHistory());
    }

    /**
     * Method will update prices history
     * @param string $feed - Yahoo Finance CSV Feed
     */
    public function updatePricesHistory($feed)
    {
        $fp = new YahooFinanceParser();
        foreach ($fp->getIChartPrices($feed) as $p) {
            $this->getPriceHistory()->addPrice($p[4], strtotime($p[0]));
            $lastPrice = $p[4];
        }

        $this->setPrice($lastPrice);
    }

    /**
     * Method will return Certificate as HTML
     * @return HTML|string
     */
    public function getCertificateAsArray()
    {
        return (array)$this;
    }

}