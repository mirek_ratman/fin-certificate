<?php
/**
 * Definition of targeting markets
 * @copyright Mirek Ratman
 * @author Mirek Ratman
 * @since 2015-07-28
 * @license The MIT License (MIT)
 */

namespace Fin\Enums;

use Fin\Helpers\Enums\TradingMarketEnumHelper;

class TradingMarket extends TradingMarketEnumHelper
{
    /*
     * Trading market list ENUM
     * @var array
     */
    protected $list = array(
        'xxsc' => array(
            'mic' => 'xxsc',
            'name' => 'Frankfurt CEF SC'
        ),
        'xlon' => array(
            'mic' => 'xlon',
            'name' => 'London Stock Exchange'
        ),
        // and more - examples taken from from http://www.iso15022.org/mic/homepagemic.htm
    );

}

