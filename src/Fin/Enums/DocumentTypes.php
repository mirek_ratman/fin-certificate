<?php
/**
 * Definition of document types
 * @copyright Mirek Ratman
 * @author Mirek Ratman
 * @since 2015-07-27
 * @license The MIT License (MIT)
 */

namespace Fin\Enums;

use Fin\Helpers\Enums\DocumentTypesEnumHelper;

class DocumentTypes extends DocumentTypesEnumHelper
{
    /*
     * Document types definition
     * @var array
     */
    protected $types = array(
        0 => array(
            'name' => 'Term Sheet',
            'ext' => 'pdf'
        ),
        1 => array(
            'name' => 'Information',
            'ext' => 'doc'
        )
        //and more if necessary....
    );

}

