<?php
/**
 * Created by PhpStorm.
 * User: mirekratman
 * Date: 28/07/15
 * Time: 10:45
 */

namespace Fin\Helpers\Parsers;

class Csv
{
    /**
     * Return CSV fileds as array
     * @param string $file - file to parse and convert
     * @return array
     */
    public function getCsvAsArray($file)
    {
        return array_map('str_getcsv', file($file));
    }
}