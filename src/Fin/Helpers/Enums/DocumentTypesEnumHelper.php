<?php
/**
 * Created by PhpStorm.
 * User: mirekratman
 * Date: 28/07/15
 * Time: 10:45
 */

namespace Fin\Helpers\Enums;

use Fin\Helpers\EnumHelper;

class DocumentTypesEnumHelper extends EnumHelper
{
    /**
     * Return all defined document types
     * @return array
     */
    public function getTypes()
    {
        $list = parent::toArray();
        if ($l = $list['types']) {
            return $l;
        }
        throw new \Exception('No document types list defined in DocumentTypes ENUM');
    }

    /**
     * Method validate if given document has right extension by its type
     * @param string $file - path to file
     * @param integer $type - ID of defined type
     * @return boolean
     */
    public function isValidType($file, $type)
    {
        $types = self::getTypes();
        $ext = pathinfo($file, PATHINFO_EXTENSION);

        if ($el = $types[$type]) {
            return $el['ext'] === $ext ? true : false;
        }
        return false;
    }

    /**
     * Return type name by given types ID
     * @param integer $id - id of document type
     * @return array
     */
    public function getTypeNameById($id)
    {
        $types = self::getTypes();
        if( isset($types[$id]) && isset($types[$id]['name']) ){
            return $types[$id]['name'];
        }
        throw new \Exception('No document type defined under provided ID');
    }

}