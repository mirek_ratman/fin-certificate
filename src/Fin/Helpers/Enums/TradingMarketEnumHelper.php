<?php
/**
 * Created by PhpStorm.
 * User: mirekratman
 * Date: 28/07/15
 * Time: 10:45
 */

namespace Fin\Helpers\Enums;

use Fin\Helpers\EnumHelper;

class TradingMarketEnumHelper extends EnumHelper
{
    /**
     * Return all defined markets list
     * @return array
     */
    public function getMarketList()
    {
        $list = parent::toArray();
        if( $l = $list['list'] ) {
            return $l;
        }
        throw new \Exception('No market list defined in TradingMarket ENUM');
    }

    /**
     * Get market name by MIC ID
     * @param string $marketId - market ID
     * @return string
     */
    public function getMarketNameById($marketId)
    {
        $list = self::getMarketList();
        if( $m = $list[$marketId] ) {
            return $m['name'];
        }
        throw new \Exception('No market ID ('. $marketId.') defined in TradingMarket ENUM');
    }

    /**
     * Get market details by given MIC ID
     * @param string $marketId - market ID
     * @return string
     */
    public function getMarketDetailsById($marketId)
    {
        $list = self::getMarketList();
        if( $m = $list[$marketId] ) {
            return $m;
        }
        throw new \Exception('No market ID ('. $marketId.') defined in TradingMarket ENUM');
    }

    /**
     * Method validate if given MIC ID number is a number of valid Trading Market
     * @param string $micId - MIC market ID to valid
     * @return boolean
     */
    public function isValidMarket($micId)
    {
        $list = self::getMarketList();

        if ($el = $list[$micId]) {
            return true;
        }
        return false;
    }

}