<?php
/**
 * Created by PhpStorm.
 * User: mirekratman
 * Date: 28/07/15
 * Time: 10:45
 */

namespace Fin\Helpers;

use Fin\Helpers\Parsers\Csv;

class YahooFinanceParser
{

    /*
     * CSV parser instance
     * @var mixed
     */
    private $csv;

    /**
     * @return mixed
     */
    public function getCsv()
    {
        return $this->csv;
    }

    /**
     * @param mixed $csv
     */
    public function setCsv($csv)
    {
        $this->csv = $csv;
    }

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->setCsv(new Csv());
    }

    /**
     * Method return prices list from Yahoo finance
     * @param string $csvFile - link/url to CSV file from Yahoo Finance iChart website
     * @return array
     */
    public function getIChartPrices($csvFile)
    {
        $list = $this->getCsv()->getCsvAsArray($csvFile);
        array_shift($list);

        return $list;
    }

}