<?php
/**
 * Bonus Certificate class definition
 * @copyright Miroslaw Ratman
 * @author Miroslaw Ratman
 * @since 2015-07-27
 * @license MIT
 */

namespace Fin;

use Fin\Certificate;

class BonusCertificate extends Certificate
{
    /*
     * Barier level
     * @var double
     */
    protected $barier;

    /**
     * @return mixed
     */
    public function getBarier()
    {
        return $this->barier;
    }

    /**
     * @param mixed $barier
     */
    public function setBarier($barier)
    {
        $this->barier = $barier;
    }

    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

}